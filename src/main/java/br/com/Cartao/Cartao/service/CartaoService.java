package br.com.Cartao.Cartao.service;

import br.com.Cartao.Cartao.clients.ClienteClient;
import br.com.Cartao.Cartao.exception.CartaoFoudException;
import br.com.Cartao.Cartao.exception.CartaoNotFoundExeption;
import br.com.Cartao.Cartao.exception.ClienteNotFoundException;
import br.com.Cartao.Cartao.models.Cartao;
import br.com.Cartao.Cartao.clients.dtoClient.ClienteDTO;
import br.com.Cartao.Cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao criarCartao(Cartao cartao){

        Optional<Cartao> optionalCartao= cartaoRepository.findByNumero(cartao.getNumero());
        if(optionalCartao.isPresent())
            throw new CartaoFoudException();

        Optional<ClienteDTO> optionalCliente = clienteClient.consultarClientePorId(cartao.getClienteId());
        cartao.setClienteId(optionalCliente.get().getId());
        return cartaoRepository.save(cartao);

    }

    public Cartao ativarDesativarCartao(int id, boolean ativo){
        Optional<Cartao> optionalCartao = consultaCartao(id);
        optionalCartao.get().setAtivo(ativo);
        return cartaoRepository.save(optionalCartao.get());
    }

    public Optional<Cartao> consultaCartao(int id){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);
        if(optionalCartao.isPresent()){
            return optionalCartao;
        }else
            throw new CartaoNotFoundExeption();
    }

//    public Optional<Cartao> consultaCartaoByClienteIdAndNumero(int clienteId, String numero, int){
//        Optional<Cartao> optionalCartao = cartaoRepository.findByClienteIdAndNumero(clienteId, numero);
//        if(optionalCartao.isPresent()){
//            return optionalCartao;
//        }else
//            throw new CartaoNotFoundExeption();
//    }

}

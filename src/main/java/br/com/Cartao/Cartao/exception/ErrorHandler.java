package br.com.Cartao.Cartao.exception;

import br.com.Cartao.Cartao.clients.ClienteClientFallback;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Feign;
import feign.FeignException;
import feign.RetryableException;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.json.JSONException;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.DataInput;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ErrorHandler {

    private String mensagemPadrao = "Campos informados incorretamente no Body";

    @ExceptionHandler(FeignException.class)
    public ResponseEntity handleFeignException(FeignException e, HttpServletResponse response)
            throws JSONException, IOException {

        HashMap<String,Object> result =
                (HashMap<String, Object>) new ObjectMapper().readValue(e.contentUTF8(),Map.class);

        return ResponseEntity.status(e.status()).body(result);
    }

    @Bean
    @ExceptionHandler(RetryableException.class)
    public Feign.Builder builder() {
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new ClienteClientFallback(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(feignDecorators);
    }

//    @ExceptionHandler(RuntimeException.class)
//    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
//    public MensagemDeErro teste(RuntimeException e){
//
//        StackTraceElement[] teste;
//        teste = e.getStackTrace();
//
//        MensagemDeErro mensagemDeErro = new MensagemDeErro(
//                new Date(),
//                e.toString(),
//                e.getMessage(),
//                teste[0].toString());
//
//        return mensagemDeErro;
//        //return ResponseEntity.status().body(mensagemDeErro);
//    }


//    @ExceptionHandler(FeignException.BadRequest.class)
//    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
//    public Map<String, Object> handleFeignBadRequestException(FeignException e, HttpServletResponse response)
//            throws JSONException, IOException {
//
//        HashMap<String,Object> result =
//                (HashMap<String, Object>) new ObjectMapper().readValue(e.contentUTF8(),Map.class);
//
//        return result;
//    }
//    @ExceptionHandler(FeignException.NotFound.class)
//    @ResponseStatus(code = HttpStatus.NOT_FOUND)
//    public Map<String, Object> handleFeignNotFoundException(FeignException e, HttpServletResponse response)
//            throws JSONException, IOException {
//
//        HashMap<String,Object> result =
//                (HashMap<String, Object>) new ObjectMapper().readValue(e.contentUTF8(),Map.class);
//
//        return result;
//    }
//    @ExceptionHandler(Exception.class)
//    @ResponseStatus(code = HttpStatus.EXPECTATION_FAILED)
//    public Map<String, Object> teste(FeignException e, HttpServletResponse response)
//            throws JSONException, IOException {
//
//        HashMap<String,Object> result =
//                (HashMap<String, Object>) new ObjectMapper().readValue(e.contentUTF8(),Map.class);
//
//        return result;
//    }
//
//
//    @ExceptionHandler(MethodArgumentNotValidException.class) // erro disparado na validação dos argumentos passados
//    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY) // informa que não foi processado por conta das entidades
//    @ResponseBody //indica que a resposta será no body da api
//    public MensagemDeErro manipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception) {
//
//        HashMap<String, ObjetoDeErro> erros = new HashMap<>();
//
//        BindingResult resultado = exception.getBindingResult();
//
//        for (FieldError erro : resultado.getFieldErrors()) {
//            erros.put(erro.getField(), new ObjetoDeErro(erro.getDefaultMessage(), erro.getRejectedValue().toString()));
//        }
//
//        MensagemDeErro mensagemDeErro = new MensagemDeErro(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
//                this.mensagemPadrao, erros);
//
//        return mensagemDeErro;
//    }
}

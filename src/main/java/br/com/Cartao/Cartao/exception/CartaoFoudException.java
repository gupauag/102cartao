package br.com.Cartao.Cartao.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cartão já cadatrado.")
public class CartaoFoudException extends RuntimeException {
}


package br.com.Cartao.Cartao.exception;

import javax.xml.crypto.Data;
import java.time.LocalDate;
import java.util.Date;

public class MensagemDeErro {
    private Date timestamp;
    private String erro;
    private String message;
    private String path;

    public MensagemDeErro(Date timestamp, String erro, String message, String path) {
        this.timestamp = timestamp;
        this.erro = erro;
        this.message = message;
        this.path = path;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

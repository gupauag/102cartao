package br.com.Cartao.Cartao.models.dtos;

import br.com.Cartao.Cartao.models.Cartao;

import java.util.Optional;

public class MappingDTO {

    public CartaoSaidaDTO mappingCartaoDTO(Cartao cartao){
        CartaoSaidaDTO saidaDTO = new CartaoSaidaDTO();
        saidaDTO.setAtivo(cartao.isAtivo());
        saidaDTO.setClienteId(cartao.getClienteId());
        saidaDTO.setNumero(cartao.getNumero());
        saidaDTO.setId(cartao.getId());
        return saidaDTO;
    }

    public CartaoSaidaGetDTO mappingCartaoDTOGet(Optional<Cartao> cartao){
        CartaoSaidaGetDTO saidaDTO = new CartaoSaidaGetDTO();
        saidaDTO.setClienteId(cartao.get().getClienteId());
        saidaDTO.setNumero(cartao.get().getNumero());
        saidaDTO.setId(cartao.get().getId());
        saidaDTO.setAtivo(cartao.get().isAtivo());
        return saidaDTO;
    }

    public Cartao mappingCartaoEntrada(CartaoEntradaDTO cartaoEntradaDTO){
        Cartao cartao = new Cartao();
        cartao.setAtivo(cartaoEntradaDTO.isAtivo());
        cartao.setClienteId(cartaoEntradaDTO.getClienteId());
        cartao.setNumero(cartaoEntradaDTO.getNumero());

        return cartao;

    }


}

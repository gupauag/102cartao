package br.com.Cartao.Cartao.repository;

import br.com.Cartao.Cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {

    Optional<Cartao> findByNumero(String numero);

    Optional<Cartao> findByClienteIdAndNumero(int clienteId, String numero);

}

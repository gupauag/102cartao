package br.com.Cartao.Cartao.controller;

import br.com.Cartao.Cartao.models.Cartao;
import br.com.Cartao.Cartao.models.dtos.CartaoEntradaDTO;
import br.com.Cartao.Cartao.models.dtos.CartaoSaidaDTO;
import br.com.Cartao.Cartao.models.dtos.CartaoSaidaGetDTO;
import br.com.Cartao.Cartao.models.dtos.MappingDTO;
import br.com.Cartao.Cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    MappingDTO mappingDTO = new MappingDTO();

    @PostMapping
    public ResponseEntity<CartaoSaidaDTO> criarCartao(@RequestBody CartaoEntradaDTO cartaoEntradaDTO){
        Cartao cartao = mappingDTO.mappingCartaoEntrada(cartaoEntradaDTO);
         CartaoSaidaDTO cartaoObjeto = mappingDTO.mappingCartaoDTO(cartaoService.criarCartao(cartao));
         return ResponseEntity.status(201).body(cartaoObjeto);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<CartaoSaidaDTO> ativarDesativarCartao(@PathVariable(name = "id") int id,
                                                        @RequestBody CartaoEntradaDTO cartaoEntradaDTO){
         Cartao cartao = mappingDTO.mappingCartaoEntrada(cartaoEntradaDTO);
         CartaoSaidaDTO cartaoObjeto = mappingDTO.mappingCartaoDTO(
                 cartaoService.ativarDesativarCartao(id, cartao.isAtivo()));
         return ResponseEntity.status(200).body(cartaoObjeto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CartaoSaidaGetDTO> consultaCartao(@PathVariable(name = "id") int id) throws Exception {
        //if(1==1) throw new Exception("Erro genérico");
        CartaoSaidaGetDTO cartaoObjeto = mappingDTO.mappingCartaoDTOGet(cartaoService.consultaCartao(id));
        return ResponseEntity.status(200).body(cartaoObjeto);
    }

    @PostMapping("/{id}/expirar")
    public ResponseEntity<CartaoSaidaDTO> expiraCartao(@PathVariable(name = "id") int id){
        CartaoSaidaDTO cartaoObjeto = mappingDTO.mappingCartaoDTO(
                cartaoService.ativarDesativarCartao(id, false));
        return ResponseEntity.status(200).body(cartaoObjeto);
    }

}

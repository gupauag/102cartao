package br.com.Cartao.Cartao.clients;

import br.com.Cartao.Cartao.clients.dtoClient.exceptions.InvalidClienteException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new InvalidClienteException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}

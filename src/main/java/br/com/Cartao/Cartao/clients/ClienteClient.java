package br.com.Cartao.Cartao.clients;

import br.com.Cartao.Cartao.clients.dtoClient.ClienteDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    Optional<ClienteDTO> consultarClientePorId(@PathVariable int id);
}

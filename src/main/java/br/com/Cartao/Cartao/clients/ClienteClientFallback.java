package br.com.Cartao.Cartao.clients;

import br.com.Cartao.Cartao.clients.dtoClient.ClienteDTO;
import br.com.Cartao.Cartao.clients.dtoClient.exceptions.OffilineClienteException;

import java.util.Optional;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Optional<ClienteDTO> consultarClientePorId(int id){
        throw new OffilineClienteException();
    }

}

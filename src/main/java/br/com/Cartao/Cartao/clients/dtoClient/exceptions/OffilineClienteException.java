package br.com.Cartao.Cartao.clients.dtoClient.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O serviço de cliente se encontra offiline")
public class OffilineClienteException extends RuntimeException {
}

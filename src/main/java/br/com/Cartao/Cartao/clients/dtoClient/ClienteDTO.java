package br.com.Cartao.Cartao.clients.dtoClient;

public class ClienteDTO {

    private int id;

    private String name;

    public ClienteDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
